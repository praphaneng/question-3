const express = require("express");
const request = require("request");
const cheerio = require("cheerio");
const app = express();
const port = 8000;
const fundsNAV = [];
var index;
var result;

app.set("view engine", "ejs");

const options = {
  url: "https://codequiz.azurewebsites.net/",
  headers: {
    Cookie: "hasCookie=true",
  },
};

function callback(error, res, body) {
  if (!error && res.statusCode == 200) {
    const $ = cheerio.load(body);
    let table = $("table").html();
    let rows = $("table tbody >tr");

    for (var i = 0; i < rows.length; i++) {
      columns = $(rows[i]).find("td");
      for (var j = 0; j < columns.length; j++) {
        if (j == 2) {
          fundsNAV.push($(columns[j]).html());
        }
      }
    }
  }

  result = fundsNAV[index];

  console.log(result);
}

module.exports = {
  "B-INCOMESSF": function () {
    index = 0;
    request(options, callback);
    return "------------------";
  },
  BM70SSF: function () {
    index = 1;
    request(options, callback);
    return "------------------";
  },
  BEQSSF: function () {
    index = 2;
    request(options, callback);
    return "------------------";
  },
  "B-FUTURESSF": function () {
    index = 3;
    request(options, callback);
    return "------------------";
  },
};

require("make-runnable/custom")({
  printOutputFrame: false,
});

app.listen(port, () => {
  console.log(`app listening at http://localhost:${port}`);
});
